import Foundation

enum Processor: String, CustomStringConvertible {
    case i5 = "Intel Core i5"
    case i7 = "Intel Core i7"
    case i9 = "Intel Core i9"
    //#..................................
    var description: String { return rawValue }
}
