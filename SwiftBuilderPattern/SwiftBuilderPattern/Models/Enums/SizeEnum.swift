import Foundation

enum Size: String, CustomStringConvertible {
    case thirteenInch = "13-inch"
    case fifteenInch = "15-inch"
    //#..................................
    var description: String { return rawValue }
}
