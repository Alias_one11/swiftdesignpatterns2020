import Foundation

class Laptop {
    
    // MARK: - ©Global-PROPERTIES
    //#..............................
    var size: Size
    var processor: Processor
    var graphics: Graphics
    //#..............................
    
    /// # Initializer
    //#...................................................
    internal init(size: Size, processor: Processor, graphics: Graphics) {
        self.size = size
        self.processor = processor
        self.graphics = graphics
    }
    //#...................................................
}/// # END Laptop

