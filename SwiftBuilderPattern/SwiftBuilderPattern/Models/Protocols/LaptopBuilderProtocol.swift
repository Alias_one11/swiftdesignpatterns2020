import Foundation

/// -----------------------------------------------------
/// “buildParts() gets invoked by the Director to
/// construct the parts of the product. getLaptop()
///  returns the finished product.”
/// -----------------------------------------------------
protocol LaptopBuilder {
    
    // MARK: - ©Global-PROPERTIES
    //#..............................
    var size: Size { get set }
    var processor: Processor { get set }
    var graphics: Graphics { get set }
    //#..............................
    
    // MARK: -∂ METHODS/FUNCTIONS
    //#...................................................
    mutating func buildParts(configurations: [String : String]) -> Void
    func getLaptop() -> Laptop
    //#...................................................
}



