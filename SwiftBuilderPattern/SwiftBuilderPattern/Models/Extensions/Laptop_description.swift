import Foundation

// MARK: -∂ extension
/// --------------------------------------------------------
/// provided a custom description usinga type extension
/// that adopts the CustomStringConvertible protocol
/// --------------------------------------------------------
extension Laptop: CustomStringConvertible {
    //__________
    var description: String {
        return """
               ---( LAPTOP SPECS )---
               *. Laptop size: \(size)
               *. Processor: \(processor)
               *. Graphics: \(graphics)
               ---( ------------ )---
               """//.trimmingCharacters(in: .whitespacesAndNewlines)    
    }
}

