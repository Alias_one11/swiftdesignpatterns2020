import Foundation

/// ---------------------------------------------------
/// implement the getLaptop() method, which returns a
/// Laptop object instantiated with the values of the
/// size, processor, and graphics properties
/// ---------------------------------------------------
extension LaptopBuilder {
    //__________
    // MARK: -∂ func getLaptop() -> Laptop
    func getLaptop() -> Laptop {
        //__________
        return Laptop(
            size: size,
            processor: processor,
            graphics: graphics)
    }
}

