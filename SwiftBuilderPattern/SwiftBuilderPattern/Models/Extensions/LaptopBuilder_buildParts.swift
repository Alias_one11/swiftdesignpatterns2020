import Foundation

// MARK: -∂ extension LaptopBuilder
/// -----------------------------------------------------
/// The logic required to set up a Laptop is the
/// same for all builders. Thus, I provide a default
/// implementation in the protocol extension.
/// -----------------------------------------------------
extension LaptopBuilder {
    // MARK: -∂ buildParts(configurations: [String : String])
    mutating func buildParts(configurations: [String : String]) {
        //__________
        /// # `size` is the key : the `cases` are the value
        if let customSize = configurations["size"] {
            //__________
            switch customSize {
            case "13-inch": size = .thirteenInch
            case "15-inch": size = .fifteenInch
            default:
                print("*. ✋🏾🛑 Invalid size... ✋🏾🛑")
            }
        }
        //#..................................
        
        /// # `processor` is the key : the `cases` are the value
        if let customProcessor = configurations["processor"] {
            //__________
            switch customProcessor {
            case "i5": processor = .i5
            case "i7": processor = .i7
            case "i9": processor = .i9
            default:
                print("*. ✋🏾🛑 Invalid processor... ✋🏾🛑")
            }
        }
        //#..................................
        
        /// # `processor` is the key : the `cases` are the value
        if let customGraphics = configurations["graphics"] {
            //__________
            switch customGraphics {
            case "Intel UHD Graphics 617": graphics = .intelUHD617
            case "Intel Iris Plus Graphics 645": graphics = .intelIrisPlus645
            case "Radeon Pro Vega 20": graphics = .radeonProVega20
            default:
                print("*. ✋🏾🛑 Invalid graphics... ✋🏾🛑")
            }
        }
        //#..................................
    }// # END buildParts
    /*...................................................*/
}/// # END Extension
