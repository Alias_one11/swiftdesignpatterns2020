import Foundation

/// ----------------------------------------------------
/// We also need a`Director`. It defines an`initializer`
/// that takes a`Builder` argument. Note that I provide
/// the`protocol` here, so that callers can pass in any
/// of the concrete types. Also, I add a private builder
/// property, that gets initialized in the initializer.
/// The `constructLaptop()` method relies on the`builder`
/// to `parse` the configuration and`create` the`parts`.
/// ----------------------------------------------------
class Director {
    // MARK: - ©PROTOCOL-PROPERTIES
    //#..............................
    private var builder: LaptopBuilder?
    //#..............................
    
    /// # Initializer
    //#...................................................
    init(builder: LaptopBuilder?) {
        self.builder = builder
    }
    //#...................................................
    
    // MARK: -∂ Helper METHODS/FUNCTIONS
    //#...................................................
    func buildLaptop(configuration: [String : String]) {
        //__________
        builder?.buildParts(configurations: configuration)
    }
    //#...................................................
}/// # END Director
