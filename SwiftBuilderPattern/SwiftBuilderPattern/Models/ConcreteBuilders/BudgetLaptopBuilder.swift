import Foundation

/// ----------------------------------------------------
/// For the BudgetLaptopBuilder, I set the size to
/// `13-inch`, the processor to Intel `Core i5`, and
/// the graphics to the `basic-graphic-card`.
/// ----------------------------------------------------
class BudgetLaptopBuilder: LaptopBuilder {
    // MARK: - ©Global-PROPERTIES
    //#..............................
    var size: Size = .thirteenInch
    var processor: Processor = .i5
    var graphics: Graphics = .intelUHD617
    //#..............................
}
