import Foundation

class HighEndLaptopBuilder: LaptopBuilder {
    // MARK: - ©Global-PROPERTIES
    //#..............................
    var size: Size = .fifteenInch
    var processor: Processor = .i9
    var graphics: Graphics = .radeonProVega20
    //#..............................
}
