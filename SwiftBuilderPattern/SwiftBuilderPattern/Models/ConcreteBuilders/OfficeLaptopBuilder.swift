import Foundation

class OfficeLaptopBuilder: LaptopBuilder {
    // MARK: - ©Global-PROPERTIES
    //#..............................
    var size: Size = .fifteenInch
    var processor: Processor = .i7
    var graphics: Graphics = .intelIrisPlus645
    //#..............................
}
