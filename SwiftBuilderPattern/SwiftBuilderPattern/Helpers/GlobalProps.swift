import Foundation

// MARK: - ©Global-PROPERTIES
//#..............................
/// # Keys
let BASEMODELKEY = "base_model"
let SIZEKEY = "size"
let GRAPHICSKEY = "graphics"
/// # Values
let BUDGETVALUE = "budget"
let OFFICEVALUE = "office"
let HIGHEND = "high_end"
let THIRTEENINCHVALUE = "13-inch"
let GRAPHICSPECSVALUE = "intel-iris-plus-graphics-645"
//#..............................
