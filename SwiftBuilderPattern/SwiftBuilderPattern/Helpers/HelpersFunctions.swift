import Foundation

/// ----------------------------------------------------
/// For each model, we need to instantiate the right
/// Builder. I define a function called createBuilder().
/// This function will be responsible to create the
/// right builder based on the configuration. It takes as
/// input the configurationand returns an object of a
/// type that conforms to the LaptopBuilder protocol.
/// ----------------------------------------------------
// MARK: -∂ createBuilder() available globally
func createBuilder(configuration: [String : String]) -> LaptopBuilder? {
    //__________
    var laptopBuilder: LaptopBuilder?
    
    guard let model = configuration["base_model"] else { return nil }
    //__________
    switch model {
    case "budget": laptopBuilder = BudgetLaptopBuilder()
    case "office": laptopBuilder = OfficeLaptopBuilder()
    case "high_end": laptopBuilder = HighEndLaptopBuilder()
    default:
        print("[ERROR]..Could not build laptop...")
    }
    
    return laptopBuilder
    
}/// # END createBuilder

//#.....................................................
// MARK: -∂ completedLaptop() Returns a completed laptop
func completedLaptop(director: Director,
                     configuration: [String : String],
                     laptopBuilder: LaptopBuilder?) {
    
    // # We invoke the director's buildLaptop() method and
    // # pass in the configuration. Finally, we retrieve the
    director.buildLaptop(configuration: configuration)
    //__________
    // # object by calling the builder's getLaptop() method.
    if let laptop = laptopBuilder?.getLaptop() {
        //__________
        print("TYPE:\(configuration.debugDescription)")
        print("\(laptop)\n")
    }
    //#..................................
}/// # END completedLaptop
