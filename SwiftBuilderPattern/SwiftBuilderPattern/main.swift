import Foundation

func main() -> Void
{
    print("\n\n[ Builder Pattern ]")
    print("-----------------------------------------------------------")
    /// - ©Example #1
    // # We create a `builder`, then instantiate a `director`
    // # by passing in the reference to the `builder`.
    var configuration = [BASEMODELKEY : BUDGETVALUE]
    var laptopBuilder = createBuilder(configuration: configuration)
    var director = Director(builder: laptopBuilder)
    
    completedLaptop(director: director,
                    configuration: configuration,
                    laptopBuilder: laptopBuilder)
    //#.....................................................
    
    /// - ©Example #2
    configuration = [BASEMODELKEY : OFFICEVALUE, SIZEKEY : THIRTEENINCHVALUE]
    laptopBuilder = createBuilder(configuration: configuration)
    director = Director(builder: laptopBuilder)
    
    completedLaptop(director: director,
                    configuration: configuration,
                    laptopBuilder: laptopBuilder)
    //#.....................................................
    
    /// - ©Example #3
    // # We will purposely pass the wrong graphics to see if
    // # the error pops up from our switch cases
    configuration = [
        BASEMODELKEY : HIGHEND,
        SIZEKEY : THIRTEENINCHVALUE,
        GRAPHICSKEY : GRAPHICSPECSVALUE//<--INVALID GRAPHICS FOR HIGHEND LAPTOPS
    ]
    
    laptopBuilder = createBuilder(configuration: configuration)
    director = Director(builder: laptopBuilder)
    
    completedLaptop(director: director,
                    configuration: configuration,
                    laptopBuilder: laptopBuilder)
    //*©-----------------------------------------------------------©*/
    
    print("-----------------------------------------------------------\n")
}
// RUNS-APP
main()
